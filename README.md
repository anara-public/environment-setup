Environment setup

1. if on windows install wsl2 first 
    - https://docs.microsoft.com/en-us/windows/wsl/install-win10#step-1---enable-the-windows-subsystem-for-linux
1. install Node Version Manager - https://github.com/nvm-sh/nvm#install--update-script
1. nvm install 12.18.1
1. nvm use 12.18.1
1. setup your ssh & gpg NOTE make sure your email matches your gpg key
    - https://docs.gitlab.com/ee/ssh/README.html#generating-a-new-ssh-key-pair
    - https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/index.html
    - https://www.39digits.com/signed-git-commits-on-wsl2-using-visual-studio-code
    - add `export GPG_TTY=$(tty)` to your .bashrc or .zshrc
1. create a personal access token for use with npm and yarnrc - make sure to grant atleast `read_api` access
    - https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token
    - export PERSONAL_ACCESS_TOKEN="your personal access token here"
1. setup `.npmrc` by running the following commands in your home directory `cd ~`
    - For NPM run
       - echo @anara-packages:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
       - echo //gitlab.com/api/v4/packages/npm/:_authToken=${PERSONAL_ACCESS_TOKEN}  >> .npmrc
1. make sure you have jq and wget installed before running the clone script
1. if on a mac update bash with `brew install bash` in order to see the colorization
1. run `wget -O - https://gitlab.com/anara-public/environment-setup/-/raw/master/clone_anara_repos.sh | bash` to clone the anara repos
    - when using wsl2 on windows make sure to run clone_anara_repos.sh from the wsl2 home directory rather than the windows drive mounted into wsl
    - https://dev.to/ajeet/so-you-have-installed-windows-subsystem-for-linux-let-s-learn-how-to-move-files-inside-of-linux-root-system-4kbo
1. create an alias in `.bashrc` or `.zshrc` for `alias anara-update="wget -O - https://gitlab.com/anara-public/environment-setup/-/raw/master/clone_anara_repos.sh | bash"`
1. find your hosts file
    - windows C:\Windows\System32\drivers\etc\hosts
    - linux /etc/hosts
1. add the following entries into the hosts file
    - 127.0.0.1  vault.default.svc.cluster.local
    - 127.0.0.1  nats-client.default.svc.cluster.local
1. install golang https://golang.org/doc/install
1. install vault https://learn.hashicorp.com/tutorials/vault/getting-started-install
1. install docker for windows/mac or install docker into wsl `https://docs.docker.com/engine/install/ubuntu/`
    - https://hub.docker.com/editions/community/docker-ce-desktop-windows/
    - https://hub.docker.com/editions/community/docker-ce-desktop-mac/
1. when developing in golang run the following scripts from `~/Anara/anara-tools/local-vault-dev-setup`
    - run vault_dev_setup.sh to build backend microservices
    - run vautl_dev_teardown.sh when done
1. instal git-cz globally `npm install git-cz --global` use the following when commiting `git add . && git-cz` or `npm run cz` for node services

### Notes
- Clone repo's by running the `anara-update` alias you created earlier
- after cloning add the remote for the seed you want to start with `git remote add seed git@gitlab.com:anara-seeds/backend/go-seed-server.git service-handler-type`
- after adding a remote run `git pull seed master`


### Creating new git repos
 - Set the following Environment Variables

    *Webpack frontend clients*
  - ASSET_PATH - e.g. `https://anara.farm/dashboard/` used by webpack for the publicPath, must have a trailing slash
  - BUCKET_PATH - e.g. `/dashboard` used by the .gitlab-ci.yml to upload to the google storage path, must start with a forward slash
