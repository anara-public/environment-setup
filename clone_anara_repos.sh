#!/bin/bash

set -e

[[ -z $1 ]] && LOCATION="$1" || LOCATION="~"

export NPM_TOKEN=$(cat ~/.npmrc | grep -o '_authToken=.*' | sed 's/_authToken=//g')

if [ -z "$NPM_TOKEN" ]; then
    echo -e "\e[31mUnable to find ~/.npmrc with an authToken\e[0m"
    exit 1
fi

for ((i=0; ; i+=1)); do
    RESULT=$(curl -Ss --header "PRIVATE-TOKEN: ${NPM_TOKEN}" "https://gitlab.com/api/v4/projects?membership=true&order_by=path&page=$i" | jq -r '.[] | @base64')
    for repo in ${RESULT}; do
        _jq() {
            echo ${repo} | base64 --decode | jq -r ${1}
        }

        # do not process archived repositories
        if [ "$(_jq '.archived')" = "false" ]; then
            cd $LOCATION
            DIR="Anara/$(_jq '.namespace.full_path')"
            NAME=$(_jq '.path')

            # Only download anara repositories
            if [[ "$(_jq '.path_with_namespace')" == "anara-"* ]]; then
                if [ ! -d "$DIR/$NAME" ]; then
                    mkdir -p $DIR
                    cd $DIR
                    echo -e "\e[36mCloning\e[0m to $DIR/$NAME"
                    git clone "$(_jq '.ssh_url_to_repo')"
                else
                    cd $DIR/$NAME
                    if [ -d .git ]; then
                        echo -e "$DIR/$NAME - \e[34malready cloned\e[0m"
                    fi
                fi
            fi
            cd $LOCATION
        else
            echo -e $(_jq '.path_with_namespace') "\e[31m- archived\e[0m"
        fi
    done

    if [ -z "$RESULT" ]; then
        echo -e "\n\n \e[32mSuccess!\e[0m - Cloned all accessible Anara repositories, I did not run git pull on any old repositories"
        break
    fi
done
